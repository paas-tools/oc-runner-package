package oc

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"
)

// Runner object encapsulates running oc commands
type Runner struct {
	binaryPath *string
}

// CreateRunner provisions a Runner object
func CreateRunner(binaryPath *string) *Runner {
	return &Runner{binaryPath}
}

// RunCommand uses the exec library to run locally using
func (oc *Runner) RunCommand(command string) (output *string, err error) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	var stringOutput string

	cmd := exec.Command(*oc.binaryPath, strings.Split(command, " ")...)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		if &stderr != nil {
			fmt.Printf("Command failed with error: '%s'\n", stderr.String())
		}
		return nil, fmt.Errorf("Error running command: '%s', error was: \n%s", command, err)
	}
	stringOutput = stdout.String()
	// If result is surrounded by quotes (either "" or ''), remove them
	if len(stringOutput) >= 2 && ((stringOutput[0] == '"' && stringOutput[len(stringOutput)-1] == '"') ||
		(stringOutput[0] == '\'' && stringOutput[len(stringOutput)-1] == '\'')) {
		stringOutput = stringOutput[1 : len(stringOutput)-1]
	}
	fmt.Printf("SUCCESS - Output for command '%s' was: '%s'\n", command, stringOutput)
	return &stringOutput, nil

}
