# OC runner package

This Go package provides an easy way to run oc commands from the command line from GO

# Requirements

The oc CLI must be locally installed

# Usage

To use it, just import `"gitlab.cern.ch/paas-tools/oc-runner-go"` and create a runner
with `oc.CreateRunner`
